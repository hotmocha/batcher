package com.homocha;

import org.springframework.transaction.annotation.Transactional;

//@Component("aa")
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Person {
    //@Autowired
    private A a;
    private String lastName;
    private String firstName;

    public Person() {
        System.out.println("Person");
    }
    public  Person(A a) {
        this.a = a;
        System.out.println("Person contructor");
    }

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Transactional
    public void func() {

    }

    @Override
    public String toString() {
        return "firstName: " + firstName + ", lastName: " + lastName;
    }
}
