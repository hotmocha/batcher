package com.homocha;

import com.homocha.empty.EmptyException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    public Person process(final Person person) throws Exception {
        final String firstName = person.getFirstName().toUpperCase();
        final String lastName = person.getLastName().toUpperCase();
        final Person transformedPerson = new Person(firstName, lastName);
        System.out.println("PersonItemProcessor Converting (" + person + ") into ("
                + transformedPerson + ")");
//
        if (StringUtils.isEmpty(firstName)) {
            throw  new EmptyException("empty firstname");
        }

        return transformedPerson;
    }

}