package com.homocha.empty;

/**
 * Created by hotmocha on 2017/10/2.
 */
public class EmptyException extends RuntimeException {
    public EmptyException(String message) {
        super(message);
    }

}
