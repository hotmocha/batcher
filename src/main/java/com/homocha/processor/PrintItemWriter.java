package com.homocha.processor;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Created by hotmocha on 2017/9/30.
 */
public class PrintItemWriter<Person> implements ItemWriter<Person> {
    public void write(List<? extends Person> list) throws Exception {
        for(int i = 0; i < list.size(); i++) {
            System.out.println("PrintItemWriter:" + list.get(i).toString());
        }
    }
}
