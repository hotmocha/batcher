package com.homocha.skip;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

/**
 * Created by hotmocha on 2017/10/2.
 */
public class ExceptionSkipPolicy implements SkipPolicy {
    private Class<? extends Exception> exceptionClassToSkip;
    public ExceptionSkipPolicy(Class<? extends Exception> exceptionClassToSkip) {
        super();
        this.exceptionClassToSkip = exceptionClassToSkip;
    }

    public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
        return exceptionClassToSkip.isAssignableFrom(
                t.getClass()
        );
    }
}
